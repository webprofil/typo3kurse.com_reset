#!/bin/bash

# Bash history löschen
rm /home/kurs/.bash_history

# Dokumente leeren
rm -R /home/kurs/Dokumente
mkdir /home/kurs/Dokumente

# Downloads leeren
rm -R /home/kurs/Downloads
mkdir /home/kurs/Downloads
rsync -a /home/kurs/.html/* /home/kurs/Downloads/html/ --exclude .git --exclude .gitignore

# Desktop leeren
rm -Rf /home/kurs/Schreibtisch
mkdir /home/kurs/Schreibtisch

# Bilder leeren
rm -Rf /home/kurs/Bilder
mkdir /home/kurs/Bilder

# Backups löschen
rm -Rf /home/kurs/*.zip
rm -Rf /home/kurs/*.tar.gz

# Papierkorb leeren
rm -Rf /home/kurs/.local/share/Trash/files/
mkdir /home/kurs/.local/share/Trash/files
rm -Rf /home/kurs/.local/share/Trash/info/
mkdir /home/kurs/.local/share/Trash/info

# Git resetten
rm -f /home/kurs/.gitconfig

# PhpStorm resetten
rm -Rf /home/kurs/PhpstormProjects
mkdir /home/kurs/PhpstormProjects
## snap
rm -Rf /home/kurs/snap/phpstorm/*
## normal
rm -Rf /home/kurs/.local/share/JetBrains
find /home/kurs/.config/JetBrains/PhpStorm*/* -not -name phpstorm.key -exec rm -r {} \;

# Browser resetten
## snap
rm -Rf /home/kurs/snap/firefox/*
rm -Rf /home/kurs/snap/chromium/*
## normal
rm -Rf /home/kurs/.mozilla
rm -Rf /home/kurs/.config/chromium/Default

# ddev resetten
ddev poweroff
ddev delete -Oy typo3kurs
rm -r -f /home/kurs/typo3kurs/

# Gnome einstellen
gsettings set org.gnome.desktop.background picture-uri file:///home/kurs/.reset/Backgroundimage.jpg
gsettings set org.gnome.desktop.session idle-delay 7200
gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 32
